import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import HomeContainer from "./pages/home/HomeContainer";

function App() {
  return (
    <div>
      <HomeContainer />
    </div>
  );
}

export default App;
