import React from "react";
import Layout from "../../components/Layout";
import Slider from "./views/Slider";

export default function HomeContainer(props) {
  return (
    <Layout>
      <div>
        <Slider />
      </div>
    </Layout>
  );
}
