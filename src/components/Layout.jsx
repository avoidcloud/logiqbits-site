import React from "react";
import { Navbar, Nav } from "react-bootstrap";

export default function Layout(props) {
  return (
    <>
      <header>
        <Navbar bg="warning" expand="lg">
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#link">Link</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
      <div>{props.children}</div>
    </>
  );
}
